/* 
 * File:   CatalsytAdaptor.h
 * Author: Jose J. Camata (camata@nacad.ufrj.br)
 *
 * Created on June 2, 2016, 11:46 AM
 */

#ifndef CATALYSTADAPTOR_H
#define	CATALYSTADAPTOR_H

#include "libmesh/libmesh.h"
#include "libmesh/mesh.h"
#include "libmesh/equation_systems.h"

#include <iostream>
#include <string>

using namespace libMesh;
using namespace std;

namespace CatalystAdaptor
{

  void mark_to_rebuild_grid();

  void Initialize(int numScripts, char* visualizationScripts[]);

  void Finalize();

  void CoProcess(EquationSystems &eq, double time, unsigned int timeStep, bool lastTimeStep);
  
}


#endif	/* CATALYSTADAPTOR_H */

