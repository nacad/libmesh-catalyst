


// C++ include files that we need
#include <iostream>
#include <algorithm>
#include <math.h>

#include <cstdlib>
#include <cstdio>

// Basic include files needed for the mesh functionality.
#include "libmesh/libmesh.h"
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/vtk_io.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/equation_systems.h"

// Define the Finite Element object.
#include "libmesh/fe.h"

// Define Gauss quadrature rules.
#include "libmesh/quadrature_gauss.h"

// Define useful datatypes for finite element
// matrix and vector components.
#include "libmesh/sparse_matrix.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/elem.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/getpot.h"

// Define the DofMap, which handles degree of freedom
// indexing.
#include "libmesh/dof_map.h"

#define LIBMESH_HAS_CATALYST 

#include "catalyst_adaptor.h"

// Bring in everything from the libMesh namespace
using namespace libMesh;



int main(int argc, char ** argv) 
{
    // Initialize libraries, 
    LibMeshInit init(argc, argv);

    GetPot command_line(argc, argv);
    
   

    // Create a mesh, with dimension to be overridden later, distributed
    // across the default MPI communicator.
    Mesh mesh(init.comm());

    // Use the MeshTools::Generation mesh generator to create a uniform
    // grid on the square [-1,1]^2  (2D) or cube [-1,-1]^3 (3D). 

    /*
    MeshTools::Generation::build_square(mesh,
             10, 10,
            -1., 1.,
            -1., 1.,
            QUAD4);
    */
   MeshTools::Generation::build_cube(mesh,
             1, 1, 1, 
            -1., 1.,
            -1., 1.,
            -1., 1.,
             PRISM15);

    // Print information about the mesh to the screen.
    // Note that 5x5 QUAD9 elements actually has 11x11 nodes,
    // so this mesh is significantly larger than the one in example 2.
    mesh.print_info();
    
   
    // Create an equation systems object.
    EquationSystems equation_systems(mesh);

#ifdef LIBMESH_HAS_CATALYST
    CatalystAdaptor::Initialize(argc,argv);
#endif


#ifdef LIBMESH_HAS_CATALYST
    
    CatalystAdaptor::CoProcess(equation_systems,0.0, 0, true);
    CatalystAdaptor::Finalize();
    
#endif
    
    
    //ExodusII_IO(mesh).write_equation_systems("out.e", equation_systems);

    
   
    // All done.
    return 0;
}

