libMesh Paraview Catalyst Adaptor 

This adaptor integrates libMesh and ParaView Catalyst to perform in situ data analysis 
and visualization.

Pre-requisites: 
Paraview Catalyst libs should be installed. Please see https://www.paraview.org/in-situ/

How to use:
Changing application Makefiles by adding the following lines:
libmesh_CXXFLAGS = $(shell $(PV_CATALYST_INSTALL)/bin/paraview-config --include vtkPVCatalyst vtkPVPythonCatalyst vtkParaViewWeb)
libmesh_LFLAGS    = $(shell $(PV_CATALYST_INSTALL)/bin/paraview-config --libs vtkUtilitiesPythonInitializer vtkPVCatalyst vtkPVPythonCatalyst vtkParaViewWeb) 






